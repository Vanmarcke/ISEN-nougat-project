package async;

import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

import fragments.QuestionFragment;
import nougat.quizappnougat.QuizActivity;
import nougat.quizappnougat.R;
import utils.PreferenceUtils;


/**
 * Created by ISEN on 05/03/2017.
 */

public class TimerAsyncTask extends AsyncTask<QuestionFragment,Void,QuestionFragment> {
    private long timeAtBeginning;
    public TimerAsyncTask() {
        super();
        timeAtBeginning = System.currentTimeMillis();
    }

    @Override
    protected QuestionFragment doInBackground(QuestionFragment... questionFragments) {

        long time;
        while(System.currentTimeMillis()-timeAtBeginning <= 1000){
                time = System.currentTimeMillis()-timeAtBeginning;
        }

        return questionFragments[0];
    }

    @Override
    protected void onPostExecute(QuestionFragment questionFragment) {

        TextView textView;
        try{
            textView = (TextView)questionFragment.getView().findViewById(R.id.numberQuestion);
            TextView timeRemaining = (TextView)questionFragment.getView().findViewById(R.id.timeRemaining);
            questionFragment.setTime(questionFragment.getTime()-1);
            textView.setText("Question n°" + questionFragment.getCurrentQuestion());
            timeRemaining.setText(String.valueOf(questionFragment.getTime())+ " secondes restantes");
            QuizActivity quizzActivity = (QuizActivity)questionFragment.getActivity();

            // GESTION DU TEMPS
            if (System.currentTimeMillis()-quizzActivity.getTimeAtBeginning()<10000 && !quizzActivity.isFinish()){
                new TimerAsyncTask().execute(questionFragment);
            }
            // TIME OUT
            else{
                // Passe à la question suivante
                if(questionFragment.getCurrentQuestion()<10){
                    questionFragment.changeQuestion(questionFragment.getCurrentQuestion()+1,quizzActivity);
                    new TimerAsyncTask().execute(questionFragment);
                }
                // Quiz fini, enregistre score
                else{
                    String score = String.valueOf(quizzActivity.getRightAnswers());
                    PreferenceUtils.setScore(score);
                    Intent intent = quizzActivity.getResultActivityIntent(score);
                    quizzActivity.finish();
                    quizzActivity.startActivity(intent);
                }
            }
        }
        catch (NullPointerException error){
            Log.d("", "onPostExecute: nul");
        }
    }
}
