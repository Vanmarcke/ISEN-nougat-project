package adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import interfaces.QuizListener;
import nougat.quizappnougat.QuizApplication;
import nougat.quizappnougat.R;

public class ListQuizAdapter extends BaseAdapter {

	// The list of items to display
	private final List<String> mListQuiz;

	// The Layout inflater
	private final LayoutInflater mInflater;

	// The listener for events
	private QuizListener mListener;

	public ListQuizAdapter(List<String> listQuiz){
		mListQuiz = listQuiz;
		mInflater = LayoutInflater.from(QuizApplication.getContext());

	}

	public void setListQuizListener(QuizListener listener){
		mListener = listener;
	}
	
	@Override
	public int getCount() {
		return null != mListQuiz ? mListQuiz.size() : 0;
	}

	@Override
	public Object getItem(int position) {
		return null != mListQuiz ? mListQuiz.get(position) : null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder holder;

		if (null == convertView) {
			convertView = mInflater.inflate(R.layout.quiz_list_item, null);

			holder = new ViewHolder(convertView);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		// Get the current item
		final String quiz = (String) getItem(position);

		holder.text.setText(quiz);

		return convertView;
	}


	private class ViewHolder {
		private TextView text;

		public ViewHolder(View view) {
			text = (TextView) view.findViewById(R.id.quizListItem);
		}
	}



}
