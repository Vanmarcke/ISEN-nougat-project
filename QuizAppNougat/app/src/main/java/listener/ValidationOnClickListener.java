package listener;

import android.view.View;
import android.widget.RadioGroup;
import android.widget.Toast;

import fragments.QuestionFragment;
import nougat.quizappnougat.QuizActivity;
import nougat.quizappnougat.R;
import utils.PreferenceUtils;
import handler.JsonHandler;

import static android.view.View.GONE;

/**
 * Created by ISEN on 04/03/2017.
 */

public class ValidationOnClickListener implements View.OnClickListener {

    private QuestionFragment mQuestionFragment;
    private QuizActivity mActivity;

    public ValidationOnClickListener(QuestionFragment questionFragment, QuizActivity activity) {

        mQuestionFragment = questionFragment;
        mActivity = activity;
    }

    @Override
    public void onClick(View view) {

        /*je mets la bonne valeur sur la variable correspondant au bouton sélectionné
            juste avant la vérification de la réponse (c'est à dire dans cette méthode)
        */

        if ((mQuestionFragment.getView().findViewById(R.id.response1)).getId() == ((RadioGroup) mQuestionFragment.getView().findViewById(R.id.radioGroup)).getCheckedRadioButtonId()) {
            mActivity.setResponseChosen(1);

        }
        if ((mQuestionFragment.getView().findViewById(R.id.response2)).getId() == ((RadioGroup) mQuestionFragment.getView().findViewById(R.id.radioGroup)).getCheckedRadioButtonId()) {
            mActivity.setResponseChosen(2);

        }
        if ((mQuestionFragment.getView().findViewById(R.id.response3)).getId() == ((RadioGroup) mQuestionFragment.getView().findViewById(R.id.radioGroup)).getCheckedRadioButtonId()) {
            mActivity.setResponseChosen(3);

        }
        if ((mQuestionFragment.getView().findViewById(R.id.response4)).getId() == ((RadioGroup) mQuestionFragment.getView().findViewById(R.id.radioGroup)).getCheckedRadioButtonId()) {
            mActivity.setResponseChosen(4);

        }
        (mQuestionFragment.getView().findViewById(R.id.button)).setClickable(false);

        if (mActivity.getResponseChosen() == Integer.valueOf(JsonHandler.getElementFromJSON(Integer.valueOf(PreferenceUtils.getQuiz()), mQuestionFragment.getCurrentQuestion() - 1, "correct", -1))) {

            Toast toast = Toast.makeText(mActivity.getApplicationContext(), "Bonne réponse !", Toast.LENGTH_SHORT);
            toast.show();
            mActivity.setRightAnswers(mActivity.getRightAnswers() + 1);
            ((RadioGroup) mQuestionFragment.getView().findViewById(R.id.radioGroup)).clearCheck();
            mActivity.setResponseChosen(0);
            (mQuestionFragment.getView().findViewById(R.id.button)).setClickable(false);
        } else {
            Toast toast = Toast.makeText(mActivity.getApplicationContext(), "Mauvaise réponse !", Toast.LENGTH_SHORT);
            toast.show();
            ((RadioGroup) mQuestionFragment.getView().findViewById(R.id.radioGroup)).clearCheck();
            mActivity.setResponseChosen(0);
            (mQuestionFragment.getView().findViewById(R.id.button)).setClickable(false);
        }
        if (mQuestionFragment.getCurrentQuestion() < 10) {
            mQuestionFragment.changeQuestion(mQuestionFragment.getCurrentQuestion() + 1, mActivity);
        } else {
            mActivity.setFinish(true);
            mQuestionFragment.getView().findViewById(R.id.radioGroup).setVisibility(GONE);
            mQuestionFragment.getView().findViewById(R.id.button).setVisibility(GONE);
            mQuestionFragment.getView().findViewById(R.id.question).setVisibility(GONE);
            String score = String.valueOf(mActivity.getRightAnswers());
            PreferenceUtils.setScore(score);
        }
    }
}
