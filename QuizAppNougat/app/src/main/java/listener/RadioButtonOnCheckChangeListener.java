package listener;

import android.view.View;
import android.widget.CompoundButton;

import nougat.quizappnougat.QuizActivity;
import nougat.quizappnougat.R;

/**
 * Created by ISEN on 04/03/2017.
 */

public class RadioButtonOnCheckChangeListener implements CompoundButton.OnCheckedChangeListener {

    private QuizActivity mActivity;
    private View mView;

    public RadioButtonOnCheckChangeListener(QuizActivity quizzActivity, View view) {

        mActivity = quizzActivity;
        mView = view;
    }


    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if (!(mView.findViewById(R.id.button)).isClickable()) {
            (mView.findViewById(R.id.button)).setClickable(true);
        }
    }
}
