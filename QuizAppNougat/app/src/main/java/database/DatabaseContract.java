package database;

import android.provider.BaseColumns;

/**
 * Created by Vivien on 02/03/2017.
 */

public class DatabaseContract implements BaseColumns{
    // FIELD NAMES
    public static final String DATE = "date";
    public static final String SCORE = "score";
    public static final String USERNAME = "username";

    // TABLE NAME
    public static final String TABLE_RESULTS = "results";

    private static final String TABLE_GENERIC_CREATE_SCRIPT_PREFFIX = "CREATE TABLE IF NOT EXISTS ";
    private static final String TABLE_IMAGE_CREATE_SCRIPT_SUFFIX = "(" + _ID + " INTEGER PRIMARY KEY, " +
            DATE + " TEXT NOT NULL, " +
            SCORE + " TEXT NOT NULL, " +
            USERNAME + " TEXT NOT NULL)";

    public static final String TABLE_RESULTS_CREATE_SCRIPT = TABLE_GENERIC_CREATE_SCRIPT_PREFFIX +
            TABLE_RESULTS +
            TABLE_IMAGE_CREATE_SCRIPT_SUFFIX;

    // The projections
    public static final String[] PROJECTION_FULL = new String[]{
            _ID,
            DATE,
            SCORE,
            USERNAME
    };

    // FIELD NAMES
    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";

    // TABLE NAME
    public static final String TABLE_USERS = "users";

    private static final String TABLE_USERS_CREATE_SCRIPT_SUFFIX = "(" + _ID + " INTEGER PRIMARY KEY, " +
            LOGIN + " TEXT NOT NULL, " +
            PASSWORD + " TEXT NOT NULL)";

    public static final String TABLE_USERS_CREATE_SCRIPT = TABLE_GENERIC_CREATE_SCRIPT_PREFFIX +
            TABLE_USERS +
            TABLE_USERS_CREATE_SCRIPT_SUFFIX;

    // The projections
    public static final String[] PROJECTION_FULL_USERS = new String[]{
            _ID,
            LOGIN,
            PASSWORD
    };
}
