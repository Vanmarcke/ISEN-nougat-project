package database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import android.util.Log;


import java.util.ArrayList;
import java.util.List;

import nougat.quizappnougat.QuizApplication;
import utils.PreferenceUtils;
import utils.Result;

/**
 * Created by Vivien on 02/03/2017.
 */

public class DatabaseManager {

    public static void writeOnDatabase (Result result) {
        final SQLiteOpenHelper sqLiteOpenHelper = new DatabaseHelper(QuizApplication.getContext());
        final SQLiteDatabase quizDatabase = sqLiteOpenHelper.getWritableDatabase();

        quizDatabase.insert(DatabaseContract.TABLE_RESULTS, "", resultToContentValues(result));


        final Cursor cursor = quizDatabase.query(DatabaseContract.TABLE_RESULTS,
                DatabaseContract.PROJECTION_FULL, null, null, null, null, null);

        while (cursor.moveToNext()) {
            Result resultdb = resultFromCursor(cursor);
            Log.i("Date " , resultdb.date);
            Log.i("Score " , resultdb.score);
            Log.i("User name", resultdb.username);



        }
        if (!cursor.isClosed()) {
            cursor.close();
        }
    }

    public static ContentValues resultToContentValues(Result result){
        final ContentValues values = new ContentValues();

        // Set the score created
        if (!TextUtils.isEmpty(result.score)){
            values.put(DatabaseContract.SCORE, result.score);
        }

        // Set the date
        if (!TextUtils.isEmpty(result.score)){
            values.put(DatabaseContract.DATE, result.date);
        }

        // Set the user name
        if (!TextUtils.isEmpty(PreferenceUtils.getLogin())){
            values.put(DatabaseContract.USERNAME, PreferenceUtils.getLogin());
        }
        return values;
    }

    public static Result resultFromCursor(Cursor c){
        if (null != c){
            final Result result = new Result();

            // Retrieve the date created
            if (c.getColumnIndex(DatabaseContract.DATE) >= 0){
                result.date = c.getString(c.getColumnIndex(DatabaseContract.DATE));
            }

            // Retrieve the score
            if (c.getColumnIndex(DatabaseContract.SCORE) >= 0){
                result.score = c.getString(c.getColumnIndex(DatabaseContract.SCORE));
            }

            // Retrieve the user name
            if (c.getColumnIndex(DatabaseContract.USERNAME) >= 0){
                result.username = c.getString(c.getColumnIndex(DatabaseContract.USERNAME));
            }

            return result;
        }
        return null;
    }

    public static void writeOnLoginDatabase (String login, String password) {
        final SQLiteOpenHelper sqLiteOpenHelper = new DatabaseHelper(QuizApplication.getContext());
        final SQLiteDatabase quizDatabase = sqLiteOpenHelper.getWritableDatabase();

        quizDatabase.insert(DatabaseContract.TABLE_USERS, "", loginToContentValues(login, password));

    }

    public static ContentValues loginToContentValues(String login, String password){
        final ContentValues values = new ContentValues();

        // Set the login
        if (!TextUtils.isEmpty(login)){
            values.put(DatabaseContract.LOGIN, login);
        }

        // Set the password
        if (!TextUtils.isEmpty(password)){
            values.put(DatabaseContract.PASSWORD, password);
        }
        return values;
    }

    public static List<String> loginFromCursor(Cursor c){
        if (null != c){
            final ArrayList<String> infos = new ArrayList<>();

            // Retrieve the login
            if (c.getColumnIndex(DatabaseContract.LOGIN) >= 0){
                infos.add(c.getString(c.getColumnIndex(DatabaseContract.LOGIN)));
            }

            // Retrieve the password
            if (c.getColumnIndex(DatabaseContract.PASSWORD) >= 0){
                infos.add(c.getString(c.getColumnIndex(DatabaseContract.PASSWORD)));
            }

            return infos;
        }
        return null;
    }
}
