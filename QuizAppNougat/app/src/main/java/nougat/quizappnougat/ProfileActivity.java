package nougat.quizappnougat;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import database.DatabaseHelper;
import database.DatabaseManager;
import database.DatabaseContract;
import utils.PreferenceUtils;
import utils.Result;


/**
 * Created by Romain on 05/03/2017.
 */

public class ProfileActivity extends Activity implements View.OnClickListener{



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        findViewById(R.id.userProfile_new_quiz).setOnClickListener(this);
        TextView text = (TextView) findViewById(R.id.userProfile_username);
        text.setText(PreferenceUtils.getLogin());

        // DATABASE PART
        int compteur = 0;
        double score = 0;
        final SQLiteOpenHelper sqLiteOpenHelper = new DatabaseHelper(QuizApplication.getContext());
        final SQLiteDatabase quizDatabase = sqLiteOpenHelper.getReadableDatabase();
        final Cursor cursor = quizDatabase.query(DatabaseContract.TABLE_RESULTS,
                DatabaseContract.PROJECTION_FULL, null, null, null, null, null);
        while (cursor.moveToNext()) {
            Result resultdb = DatabaseManager.resultFromCursor(cursor);
            if (resultdb.username.equals(PreferenceUtils.getLogin())){
                compteur++;
                score += Integer.valueOf(resultdb.score);
            }
        }
        // Display number of quiz played
        TextView compte = (TextView) findViewById(R.id.userProfile_number_of_quiz);
        compte.setText(String.valueOf(compteur));

        // Display the ration of good answers
        double ratio_double = Math.round((score/(compteur*10))*100);
        String ratio_string = String.valueOf(ratio_double) + " %";
        TextView ratio = (TextView) findViewById(R.id.userProfile_ratio);
        ratio.setText(ratio_string);


    }

    @Override
    public void onClick(View v) {
        this.finish();
    }
}
