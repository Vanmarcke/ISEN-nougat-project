package nougat.quizappnougat;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

/**
 * Created by Vivien on 08/03/2017.
 */

public class StartQuizActivity extends Activity implements View.OnClickListener{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_quiz);
        findViewById(R.id.btm).setOnClickListener(this);
        findViewById(R.id.activity_listquiz).setOnClickListener(this);
        findViewById(R.id.hardpush).setVisibility(View.INVISIBLE);
    }

    @Override
    public void onClick(View v) {
        findViewById(R.id.hardpush).setVisibility(View.VISIBLE);
        final Intent intent = new Intent(this, QuizActivity.class);
        startActivity(intent);
        finish();
    }
}
