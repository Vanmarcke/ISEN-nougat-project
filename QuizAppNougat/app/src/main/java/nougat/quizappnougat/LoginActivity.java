package nougat.quizappnougat;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import database.DatabaseContract;
import database.DatabaseHelper;
import database.DatabaseManager;
import utils.Constants;
import utils.PreferenceUtils;
import utils.Result;

/**
 * Created by Vivien on 01/03/2017.
 */

public class LoginActivity extends Activity implements View.OnClickListener{

    private EditText mLoginEditText;

    private EditText mPasswordEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mLoginEditText = (EditText) findViewById(R.id.loginEditText);
        mPasswordEditText = (EditText) findViewById(R.id.passwordEditText);

        findViewById(R.id.loginButton).setOnClickListener(this);

        final String storedLogin = PreferenceUtils.getLogin();
        final String storedPassword = PreferenceUtils.getPassword();
        if ((!TextUtils.isEmpty(storedLogin)) && (!TextUtils.isEmpty(storedPassword))){
            final Intent homeIntent = getHomeActivityIntent(storedLogin);
            startActivity(homeIntent);
        }
    }

    private Intent getHomeActivityIntent(String userName){
        final Intent intent = new Intent(this, ListQuizActivity.class);
        final Bundle extras = new Bundle();
        extras.putString(Constants.Login.LOGIN, userName);
        intent.putExtras(extras);
        return intent;
    }

    @Override
    public void onClick(View v) {
        if (TextUtils.isEmpty(mLoginEditText.getText())){
            // Display a Toast to the user
            Toast.makeText(this, R.string.error_no_login, Toast.LENGTH_LONG).show();
            return;
        }

        // Check if a password is set
        if (TextUtils.isEmpty(mPasswordEditText.getText())){
            // Display a Toast to the user
            Toast.makeText(this, R.string.error_no_password, Toast.LENGTH_LONG).show();
            return;
        }

        final SQLiteOpenHelper sqLiteOpenHelper = new DatabaseHelper(QuizApplication.getContext());
        final SQLiteDatabase quizDatabase = sqLiteOpenHelper.getReadableDatabase();
        final Cursor cursor = quizDatabase.query(DatabaseContract.TABLE_USERS,
                DatabaseContract.PROJECTION_FULL_USERS, null, null, null, null, null);
        List<String> infos = new ArrayList<>();
        while (cursor.moveToNext()) {
            infos = DatabaseManager.loginFromCursor(cursor);
            if (!infos.get(0).equals(String.valueOf(mLoginEditText.getText()))){
                infos.clear();
            }
        }

        if (infos.isEmpty()){
            DatabaseManager.writeOnLoginDatabase(String.valueOf(mLoginEditText.getText()), String.valueOf(mPasswordEditText.getText()));
            infos.add(String.valueOf(mLoginEditText.getText()));
            infos.add(String.valueOf(mPasswordEditText.getText()));
        }
        else if (!infos.get(1).equals(String.valueOf(mPasswordEditText.getText()))){
            Toast.makeText(this, R.string.error_authentification, Toast.LENGTH_LONG).show();
            return;
        }

        PreferenceUtils.setLogin(mLoginEditText.getText().toString());
        PreferenceUtils.setPassword(mPasswordEditText.getText().toString());

        new Thread(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                    }
                });
            }
        }).start();

        final Intent intent = getHomeActivityIntent(mLoginEditText.getText().toString());
        startActivity(intent);
    }
}
