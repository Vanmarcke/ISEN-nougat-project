package nougat.quizappnougat;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import fragments.QuestionFragment;
import utils.Constants;

/**
 * Created by ISEN on 02/03/2017.
 */

public class QuizActivity extends Activity{

    private int responseChosen,rightAnswers;
    private long timeAtBeginning;
    private boolean isFinish;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quizz);

        QuestionFragment myFragment = new QuestionFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(R.id.container,myFragment);
        transaction.commit();



        responseChosen = 0;
        rightAnswers = 0;

        this.timeAtBeginning = System.currentTimeMillis();
    }
    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }


    public int getResponseChosen() {
        return responseChosen;
    }

    public void setResponseChosen(int responseChosen) {
        this.responseChosen = responseChosen;
    }

    public int getRightAnswers() {
        return rightAnswers;
    }

    public void setRightAnswers(int rightAnswers) {
        this.rightAnswers = rightAnswers;
    }

    public long getTimeAtBeginning() {
        return timeAtBeginning;
    }

    public void setTimeAtBeginning(long timeAtBeginning) {
        this.timeAtBeginning = timeAtBeginning;
    }

    public boolean isFinish() {
        return isFinish;
    }

    public void setFinish(boolean finish) {
        isFinish = finish;
    }

    public Intent getResultActivityIntent(String score){
        final Intent intent = new Intent(this, ResultActivity.class);
        final Bundle extras = new Bundle();
        extras.putString(Constants.Login.LOGIN, score);
        intent.putExtras(extras);
        return intent;
    }
}
