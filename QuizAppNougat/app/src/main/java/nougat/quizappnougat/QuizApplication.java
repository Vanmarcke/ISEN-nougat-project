package nougat.quizappnougat;

import android.content.Context;

/**
 * Created by ISEN on 02/03/2017.
 */

public class QuizApplication extends android.app.Application {
    private static Context sContext;

    public void onCreate(){
        super.onCreate();

        // Keep a reference to the application context
        sContext = getApplicationContext();
    }

    // Used to access Context anywhere within the app
    public static Context getContext() {
        return sContext;
    }
}
