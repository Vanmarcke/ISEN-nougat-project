package nougat.quizappnougat;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.Date;

import database.DatabaseManager;
import utils.PreferenceUtils;
import utils.Result;


/**
 * Created by ISEN on 02/03/2017.
 */


public class ResultActivity extends Activity implements View.OnClickListener{


    @TargetApi(Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_result);
        testResult(PreferenceUtils.getScore());

        findViewById(R.id.result_new_quiz).setOnClickListener(this);
        findViewById(R.id.result_user_profile).setOnClickListener(this);


        // DATABASE PART
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        sdf.setTimeZone(android.icu.util.TimeZone.getDefault());
        String date = sdf.format(new Date());
        Result result = new Result(date,PreferenceUtils.getScore(), PreferenceUtils.getLogin());
        DatabaseManager.writeOnDatabase(result);
    }

    public void testResult(String result){
        TextView text = (TextView) findViewById(R.id.text_result);

        switch(result){
            case "0":
                text.setText("Vous avez fait un score de 0, score inadmissible");
                break;
            case "1":
                text.setText("Vous avez fait un score de 1, score très faible");
                break;
            case "2":
                text.setText("Vous avez fait un score de 2, score faible");
                break;
            case "3":
                text.setText("Vous avez fait un score de 3, score assez faible");
                break;
            case "4":
                text.setText("Vous avez fait un score de 4, score moyen");
                break;
            case "5":
                text.setText("Vous avez fait un score de 5, score moyen");
                break;
            case "6":
                text.setText("Vous avez fait un score de 6, score moyen");
                break;
            case "7":
                text.setText("Vous avez fait un score de 7, bien");
                break;
            case "8":
                text.setText("Vous avez fait un score de 8, très bien");
                break;
            case "9":
                text.setText("Vous avez fait un score de 9, excellent");
                break;
            case "10":
                text.setText("Vous avez fait un score de 10, parfait");
                break;
            default:
                text.setText("Error");
                break;
        }
    }


    @Override
    public void onClick(View v) {
        final Intent intent;
        switch (v.getId()) {

            case R.id.result_user_profile:
                intent = new Intent(this, ProfileActivity.class);
                startActivity(intent);
                break;

            default:
                break;
        }
        this.finish();
    }

}
