package nougat.quizappnougat;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import fragments.ListQuizFragment;
import interfaces.QuizListener;
import utils.Constants;
import utils.PreferenceUtils;

public class ListQuizActivity extends AppCompatActivity implements QuizListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_quiz);

        final Intent intent = getIntent();
        if (null != intent) {
            final Bundle extras = intent.getExtras();
            if ((null != extras) && (extras.containsKey(Constants.Login.LOGIN))) {
                // Retrieve the login
                final String login = extras.getString(Constants.Login.LOGIN);

                // Set as ActionBar subtitle
                getSupportActionBar().setSubtitle(login);
            }
        }

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction().add(R.id.container, new ListQuizFragment()).commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        getMenuInflater().inflate(R.menu.profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.actionLogout) {
            // Erase login and password in Preferences
            PreferenceUtils.setLogin(null);
            PreferenceUtils.setPassword(null);

            // Finish this activity, and go back to LoginActivity
            this.finish();

            return true;
        }
        else if (id == R.id.actionProfile){
            Intent intent = new Intent(this, ProfileActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClickListQuiz(String quiz) {
        final Intent intent = new Intent(this, StartQuizActivity.class);
        final Bundle extras = new Bundle();
        extras.putString(Constants.ListQuiz.QUIZSELECTED, quiz);
        intent.putExtras(extras);
        startActivity(intent);
    }
}
