package handler;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import nougat.quizappnougat.QuizApplication;

/**
 * Created by Romain on 07/03/2017.
 */

public class JsonHandler {
    private static String loadJSONFromAsset() {
        String json;
        try {
            InputStream is = QuizApplication.getContext().getAssets().open("Quiz.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public static String getElementFromJSON(int numero_quiz, int numero_question, String element, int sous_index) {
        try {
            String quiz = String.valueOf(numero_quiz);
            String json = loadJSONFromAsset();
            JSONObject jObj = new JSONObject(json);
            // Get the Quiz - Array
            JSONArray subObj = jObj.getJSONArray(quiz);
            // Get the Question - Object
            JSONObject question = subObj.getJSONObject(numero_question+1);

            String output;
            // Get element inside the Question
            // Cas on veut answers - Array
            if (sous_index != -1) {
                JSONArray answers = question.getJSONArray("answers");
                output = answers.getString(sous_index);
            }
            // Cas on veut autre chose - Object
            else {
                output = question.getString(element);
            }
            Log.i("JSON", output);
            return (output);

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<String> getListQuizFromJSON(){
        ArrayList<String> listQuiz = new ArrayList<>();
        try {
            int cpt = 0;
            String json = loadJSONFromAsset();
            String strcpt;
            while (cpt < 100){
                strcpt = String.valueOf(cpt);
                JSONObject jObj = new JSONObject(json);
                // Get the Quiz - Array
                JSONArray subObj = jObj.getJSONArray(strcpt);
                // Get the Question - Object
                JSONObject quizName = subObj.getJSONObject(0);

                String output;
                output = quizName.getString("quizName");
                Log.i("JSON", output);
                listQuiz.add(output);
                Log.d("cpt", strcpt);
                cpt++;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return (listQuiz);
    }


}
