package fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import nougat.quizappnougat.QuizActivity;
import nougat.quizappnougat.R;
import listener.RadioButtonOnCheckChangeListener;
import async.TimerAsyncTask;
import listener.ValidationOnClickListener;
import utils.PreferenceUtils;

/**
 * Created by ISEN on 02/03/2017.
 */

public class QuestionFragment extends Fragment {

    private TimerAsyncTask timerAsyncTask;
    private int currentQuestion,time;
    private View mView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View viewRoot = inflater.inflate(R.layout.question_fragment, container, false);
        mView = viewRoot;

        QuizActivity myActivity = (QuizActivity) getActivity();

        Button validationButton = (Button) viewRoot.findViewById(R.id.button);
        validationButton.setOnClickListener(new ValidationOnClickListener(this,myActivity));
        validationButton.setClickable(false);

        currentQuestion = 1;
        Log.d("quiz", PreferenceUtils.getQuiz());
        int quizSelected = Integer.valueOf(PreferenceUtils.getQuiz());


        TextView textView = (TextView) viewRoot.findViewById(R.id.question);
        textView.setText(handler.JsonHandler.getElementFromJSON(quizSelected,0,"question",-1));

        RadioButton radioButton = (RadioButton) viewRoot.findViewById(R.id.response1);
        radioButton.setText(handler.JsonHandler.getElementFromJSON(quizSelected,0,"answers",0));
        radioButton.setOnCheckedChangeListener(new RadioButtonOnCheckChangeListener(myActivity,viewRoot));

        radioButton = (RadioButton) viewRoot.findViewById(R.id.response2);
        radioButton.setText(handler.JsonHandler.getElementFromJSON(quizSelected,0,"answers",1));
        radioButton.setOnCheckedChangeListener(new RadioButtonOnCheckChangeListener(myActivity,viewRoot));

        radioButton = (RadioButton) viewRoot.findViewById(R.id.response3);
        radioButton.setText(handler.JsonHandler.getElementFromJSON(quizSelected,0,"answers",2));
        radioButton.setOnCheckedChangeListener(new RadioButtonOnCheckChangeListener(myActivity,viewRoot));

        radioButton = (RadioButton) viewRoot.findViewById(R.id.response4);
        radioButton.setText(handler.JsonHandler.getElementFromJSON(quizSelected,0,"answers",3));
        radioButton.setOnCheckedChangeListener(new RadioButtonOnCheckChangeListener(myActivity,viewRoot));


        time =10;
        timerAsyncTask = new TimerAsyncTask();
        timerAsyncTask.execute(this);

        return viewRoot;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
        timerAsyncTask.cancel(true);
    }

    public int getCurrentQuestion() {
        return currentQuestion;
    }

    public void changeQuestion(int numberOfQuestion, QuizActivity myActivity){

        myActivity.setTimeAtBeginning(System.currentTimeMillis());
        this.setTime(10);
        this.currentQuestion = numberOfQuestion;

        Log.d("quiz", PreferenceUtils.getQuiz());
        int quizSelected = Integer.valueOf(PreferenceUtils.getQuiz());

        // JSON PART
        TextView textView = (TextView) mView.findViewById(R.id.question);
        textView.setText(handler.JsonHandler.getElementFromJSON(quizSelected,numberOfQuestion-1,"question",-1));

        textView = (TextView) mView.findViewById(R.id.numberQuestion);
        textView.setText("Question n°" + numberOfQuestion);

        RadioButton radioButton = (RadioButton) mView.findViewById(R.id.response1);
        radioButton.setText(handler.JsonHandler.getElementFromJSON(quizSelected,numberOfQuestion-1,"answers",0));

        radioButton = (RadioButton) mView.findViewById(R.id.response2);
        radioButton.setText(handler.JsonHandler.getElementFromJSON(quizSelected,numberOfQuestion-1,"answers",1));

        radioButton = (RadioButton) mView.findViewById(R.id.response3);
        radioButton.setText(handler.JsonHandler.getElementFromJSON(quizSelected,numberOfQuestion-1,"answers",2));

        radioButton = (RadioButton) mView.findViewById(R.id.response4);
        radioButton.setText(handler.JsonHandler.getElementFromJSON(quizSelected,numberOfQuestion-1,"answers",3));

    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getTime() {
        return time;
    }
}
