package fragments;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import adapter.ListQuizAdapter;
import handler.JsonHandler;
import interfaces.QuizListener;
import nougat.quizappnougat.R;
import utils.PreferenceUtils;


public class ListQuizFragment extends Fragment implements OnItemClickListener {

    // Keep a reference to the ListView
    private ListView mListView;

    private List<String> mlistQuiz;

    private ListQuizAdapter mListQuizAdapter;



    public ListQuizFragment() {
        mlistQuiz = JsonHandler.getListQuizFromJSON();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_list_quiz, container, false);

        // Get the ListView
        mListView = (ListView) rootView.findViewById(R.id.quizListView);

        // Set a Progress Bar as empty view, and display it (set adapter with no elements))
        final ProgressBar progressBar = new ProgressBar(getActivity());
        progressBar.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, Gravity.CENTER));
        progressBar.setIndeterminate(true);
        mListView.setEmptyView(progressBar);

        // Add the view in our content view
        ViewGroup root = (ViewGroup) rootView.findViewById(R.id.ListQuizRelativeLayout);
        root.addView(progressBar);

        // Set adapter with no elements to let the ListView display the empty view
        mListView.setAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, new ArrayList<String>()));

        // Add a listener when an item is clicked
        mListView.setOnItemClickListener(this);

        mListQuizAdapter = new ListQuizAdapter(mlistQuiz);
        mListQuizAdapter.setListQuizListener(mListener);
        mListView.setAdapter(mListQuizAdapter);

        return rootView;
    }

    // Keep a reference to our Activity (implementing QuizListener)
    private QuizListener mListener;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (activity instanceof QuizListener) {
            mListener = (QuizListener) activity;
        }
    }

    @Override
    public void onStart() {
        super.onStart();

    }


    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
        if (null != mListener) {
            Log.d("itemclick", String.valueOf(position));
            PreferenceUtils.setQuiz(String.valueOf(position));
            final String listQuiz = (String) adapter.getItemAtPosition(position);
            mListener.onClickListQuiz(listQuiz);
        }
    }
}
