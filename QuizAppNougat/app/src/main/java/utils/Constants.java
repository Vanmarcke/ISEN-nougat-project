package utils;

/**
 * Created by Vivien on 01/03/2017.
 */

public class Constants {
    public class Login {
        public static final String LOGIN = "login";
    }

    public class Preferences {
        public static final String SHARED_PREFERENCES_FILE_NAME = "sharedPreferences";
        public static final String PREF_LOGIN = "prefLogin";
        public static final String PREF_PASSWORD = "prefPassword";
        public static final String PREF_SCORE = "prefScore";
        public static final String QUIZSELECTED = "quizselected";
    }

    public class ListQuiz {
        public static final String QUIZSELECTED = "quizselected";
    }
}
