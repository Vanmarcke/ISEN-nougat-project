package utils;

/**
 * Created by Romain on 05/03/2017.
 */


public class Result {



    public String date;

    public String score;

    public String username;

    public Result(String date, String score, String username) {
        this.date = date;
        this.score = score;
        this.username = username;
    }

    public Result(){}
}
