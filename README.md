# QuizAppNougat

Bienvenue sur la page du projet android : QuizzAppNougat

## Fonctionnalités

Lorsque l’utilisateur lance l’application pour la première fois, il arrive sur une page de connexion (le nom et le mot de passe seront enregistrés pour une connexion ultérieure). 
Une fois les informations demandées remplies (nom et mot de passe), l’utilisateur peut choisir parmi une liste de quiz celui qu’il veut faire. Il peut également accéder à son profil.  
Lorsqu’il clique sur un quiz, celui-ci est chargé est l’utilisateur peut répondre à la série de 10 questions. Pour chaque question il dispose de 10 secondes pour répondre afin d’empêcher une éventuelle triche sur internet. S’il n’est pas assez rapide, il ne marque pas de point et on passe à la question d’après. A chaque fois que l’utilisateur valide une question, un toast lui indique si sa réponse était bonne ou mauvaise. 
Enfin, quand l’utilisateur a fini de répondre aux 10 questions, il accède à une page de résultat qui lui donne son score et qui permet d’accéder à un nouveau quiz ou à sa page de profil. 
Une page de profil indique à l’utilisateur combien de quiz il a effectué et calcule le ratio de bonnes réponses parmi toutes les questions.  
Nous avons également une liste de fonctionnalités que nous aurions aimé ajouter mais que nous n’avons pas encore eu le temps : 
- Une page réservée au professeur, pour qu’il puisse consulter les profils de ses étudiants et rajouter des quiz. 
- Un partage des résultats avec d’autres utilisateurs 
- Une page de réglages (pour régler la durée du timer par exemple)
- Un historique plus détaillé des quiz effectué (pouvoir voir la bonne réponse, la réponse que nous avons donné etc…) 

## Team

BENDRISS MOHAMMED   
DONG XUCHEN   
DUVAL MARC-ANTOINE   
HALLOY GUILLAUME   
GEVERS MARC   
GORSKI EMERIC   
LARIVE VINCENT   
TAHRI JOUTEI HASSANI MOHAMMED   
VALENCOURT VIVIEN   
VANMARCKE ROMAIN   
 